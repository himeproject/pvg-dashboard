import { WrapperLanding } from "./styles";
import Board from "../../features/board";
import Layout from "../../features/layout";
import Menus from "../../features/menus";
import Profile from "../../features/profile";
import React from "react";

const LandingView = () => {
  return (
    <Layout>
      <WrapperLanding>
        <Menus />
        <Board />
        <Profile />
      </WrapperLanding>
    </Layout>
  );
};

export default LandingView;
