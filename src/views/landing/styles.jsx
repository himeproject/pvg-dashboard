import styled from "styled-components";

export const WrapperLanding = styled.div`
  height: 100vh;
  display: flex;

  @media only screen and (max-width: 768px) {
    flex-wrap: wrap;
  }
`;
