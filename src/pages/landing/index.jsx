import React from "react";
import LandingView from "../../views/landing";

const LandingPage = () => {
  return (
    <>
      <LandingView />
    </>
  );
};

export default LandingPage;
