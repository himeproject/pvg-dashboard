import styled from "styled-components";

export const WrapperNavigation = styled.ul`
  padding: 0;
  display: flex;
  flex-direction: column;
  gap: 47px 0;
  padding: 0 0 0 52px;

  > li {
    position: relative;
    display: flex;
    align-items: center;
    gap: 31px 20px;
    flex-wrap: wrap;
    cursor: pointer;

    > .signed {
      position: absolute;
      right: 0;
      top: -10%;
    }

    > .collapse {
      position: absolute;
      right: 10%;
      top: 0;
    }
  }
`;

export const WrapperChildMenu = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  gap: 31px 0;
  padding: 0 0 0 42px;
  cursor: pointer;
`;

export const WrapperLogout = styled.div`
  position: relative;
  display: flex;
  gap: 0 20px;
  cursor: pointer;

  > hr {
    position: absolute;
    border-bottom: 2px solid #c4c4c4;
    border-top: none;
    width: 100%;
    top: -100%;
    left: -10%;
  }
`;
