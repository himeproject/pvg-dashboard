import { WrapperNavigation, WrapperChildMenu, WrapperLogout } from "./styles";
import React from "react";
import balanceIcon from "../../assets/balance.svg";
import channelIcon from "../../assets/channel.svg";
import collapseIcon from "../../assets/collapse.svg";
import dashboardIcon from "../../assets/dashboard.svg";
import logoutIcon from "../../assets/logout.svg";
import productIcon from "../../assets/product.svg";
import signedIcon from "../../assets/signed.svg";
import transactionIcon from "../../assets/transaction.svg";

const Navigation = () => {
  const menus = [
    {
      name: "Dashboard",
      icon: dashboardIcon,
    },
    {
      name: "Balance",
      icon: balanceIcon,
    },
    {
      name: "Transaction",
      icon: transactionIcon,
    },
    {
      name: "Product",
      icon: productIcon,
    },
    {
      name: "Channel",
      icon: channelIcon,
      child: ["Agent", "Balance", "Transaction"],
    },
  ];

  const renderSigned = (name) => {
    if (name !== "Dashboard") return;
    return (
      <figure className="signed">
        <img src={signedIcon} alt="Signed Icon" />
      </figure>
    );
  };

  return (
    <WrapperNavigation>
      {menus.map((menu, index) => (
        <li key={index}>
          {renderSigned(menu.name)}
          <img src={menu.icon} alt={`${menu.name} Icon`} />
          <p>{menu.name}</p>
          {menu.child && (
            <>
              <figure className="collapse">
                <img src={collapseIcon} alt="Collapse Icon" />
              </figure>
              <WrapperChildMenu>
                {menu.child?.map((item, index) => (
                  <p key={index}>{item}</p>
                ))}
              </WrapperChildMenu>
            </>
          )}
        </li>
      ))}
      <WrapperLogout>
        <hr />
        <img src={logoutIcon} alt="Logout Icon" />
        <p>Logout</p>
      </WrapperLogout>
    </WrapperNavigation>
  );
};

export default Navigation;
