import { WrapperMenus } from "./styles";
import Navigation from "./components/Navigations";
import React from "react";

const Menus = () => {
  return (
    <WrapperMenus>
      <h1>SalesDash</h1>
      <Navigation />
    </WrapperMenus>
  );
};

export default Menus;
