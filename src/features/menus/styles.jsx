import styled from "styled-components";

export const WrapperMenus = styled.div`
  width: 15%;
  height: 100%;
  background: #fcfcff;

  > h1 {
    font-style: normal;
    font-weight: 700;
    font-size: 29px;
    line-height: 40px;
    text-align: center;
    color: #545dff;
    padding: 45px 0 68px;
  }

  @media only screen and (max-width: 768px) {
    width: 100%;
    display: none;
  }
`;
