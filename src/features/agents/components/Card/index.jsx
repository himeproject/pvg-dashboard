import { Table, Wrapper } from "./styles";
import CountUp from "react-countup";
import React from "react";

const Card = ({ company, profit, sales, transactions }) => {
  const randomRange = (min, max) => {
    return ~~(Math.random() * (max - min + 1)) + min;
  };

  return (
    <Wrapper>
      <figure>
        <img
          src={`https://i.pravatar.cc/60?img=${randomRange(1, 60)}`}
          alt="Profile"
        />
      </figure>
      <div className="card">
        <p className="company">{company}</p>
        <Table>
          <tbody>
            <tr>
              <td className="title">Profit</td>
              <CountUp
                start={0}
                end={profit}
                duration={3}
                separator="."
                delay={0}
                prefix="Rp "
              >
                {({ countUpRef, start }) => (
                  <td ref={countUpRef} className="value" />
                )}
              </CountUp>
            </tr>
            <tr>
              <td className="title">Sales</td>
              <CountUp
                start={0}
                end={sales}
                duration={3}
                separator="."
                delay={0}
                prefix="Rp "
              >
                {({ countUpRef, start }) => (
                  <td ref={countUpRef} className="value" />
                )}
              </CountUp>
            </tr>
            <tr>
              <td className="title">Transactions</td>
              <td className="value-transaction">
                <CountUp
                  start={0}
                  end={transactions}
                  duration={3}
                  separator="."
                  delay={0}
                >
                  {({ countUpRef, start }) => (
                    <p ref={countUpRef} className="value" />
                  )}
                </CountUp>
                <span>transactions</span>
              </td>
            </tr>
          </tbody>
        </Table>
      </div>
    </Wrapper>
  );
};

export default Card;
