import styled from "styled-components";

export const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 8px;
  align-items: center;
  justify-content: center;

  figure {
    > img {
      border: 3px solid #989eff;
      border-radius: 180%;
    }
  }

  > .card {
    > .company {
      font-style: normal;
      font-weight: 700;
      font-size: 16px;
      line-height: 22px;
      color: #545dff;
    }
  }

  @media only screen and (max-width: 768px) {
    width: fit-content;
  }
`;

export const Table = styled.table`
  > tbody {
    > tr {
      > .title {
        font-style: normal;
        font-weight: 600;
        font-size: 14px;
        line-height: 19px;
        color: #7a7a7a;
      }

      > .value,
      .value-transaction {
        font-style: normal;
        font-weight: 700;
        font-size: 14px;
        line-height: 19px;
        color: #212121;
      }

      > .value-transaction {
        display: flex;
        gap: 4px;
      }
    }
  }
`;
