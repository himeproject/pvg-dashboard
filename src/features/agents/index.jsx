import { WrapperAgent } from "./styles";
import React from "react";
import Card from "./components/Card";

const Agents = () => {
  const agents = [
    {
      company: "PT Suka Maju Jakarta",
      profit: 3700000,
      sales: 7230000,
      transactions: 43,
    },
    {
      company: "Susi Susanti",
      profit: 3550000,
      sales: 5230000,
      transactions: 37,
    },
    {
      company: "Toko Pulsa III Depok",
      profit: 2860000,
      sales: 4500000,
      transactions: 38,
    },
    {
      company: "PT Suka Maju Bogor",
      profit: 2400000,
      sales: 4800000,
      transactions: 40,
    },
    {
      company: "Anton Antoman",
      profit: 2350000,
      sales: 2100000,
      transactions: 20,
    },
  ];

  return (
    <WrapperAgent>
      <p className="title">Top 5 Agents</p>
      <div className="list-agents">
        {agents.map((agent, index) => (
          <Card
            company={agent.company}
            key={index}
            profit={agent.profit}
            transactions={agent.transactions}
            sales={agent.sales}
          />
        ))}
      </div>
    </WrapperAgent>
  );
};

export default Agents;
