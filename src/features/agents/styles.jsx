import styled from "styled-components";

export const WrapperAgent = styled.div`
  width: calc(100% / 3 - 8px);
  margin: 40px 0 0;
  background: #ffffff;
  box-shadow: 0px 0px 26px #f0f1ff;
  border-radius: 12px;
  padding: 18px 16px;

  > .title {
    font-style: normal;
    font-weight: 700;
    font-size: 16px;
    line-height: 22px;
    color: #7a7a7a;
    margin: 0 0 51px;
  }

  > .list-agents {
    height: calc(100% - 60px);
    display: flex;
    flex-direction: column;
    gap: 21px;
    overflow-y: scroll;
    overflow-x: unset;
  }

  @media only screen and (max-width: 768px) {
    width: 100%;
    height: fit-content;

    > .list-agents {
      height: 100%;
      flex-direction: row;
      flex-wrap: wrap;
      overflow-y: scroll;
      overflow-x: unset;
    }
  }
`;
