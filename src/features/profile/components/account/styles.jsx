import styled from "styled-components";

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  gap: 8px;

  > .profile-picture {
    > img {
      border: 3px solid #989eff;
      border-radius: 180%;
    }
  }

  > div {
    > .name {
      font-style: normal;
      font-weight: 700;
      font-size: 20px;
      line-height: 27px;
      color: #212121;
    }

    > .title {
      font-style: normal;
      font-weight: 700;
      font-size: 16px;
      line-height: 22px;
      color: #7a7a7a;
    }
  }
`;
