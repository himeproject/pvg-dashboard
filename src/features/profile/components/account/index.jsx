import { Wrapper } from "./styles";
import React from "react";
import expandIcon from "../../assets/expand.svg";

const Account = () => {
  return (
    <Wrapper className="account">
      <figure className="profile-picture">
        <img src="https://i.pravatar.cc/60" alt="Admin" />
      </figure>
      <div>
        <p className="name">Budi Budiman</p>
        <p className="title">Owner at PT Suka Maju</p>
      </div>
      <figure>
        <img src={expandIcon} alt="Expand Icon" />
      </figure>
    </Wrapper>
  );
};
export default Account;
