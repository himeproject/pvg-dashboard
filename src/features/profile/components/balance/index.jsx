import { Wrapper } from "./styles";
import CountUp from "react-countup";
import React from "react";
import optionIcon from "../../assets/option.svg";

const Balance = () => {
  return (
    <Wrapper>
      <figure>
        <img src={optionIcon} alt="Option Icon" />
      </figure>
      <p className="title">Total Balance</p>
      <CountUp
        start={0}
        end={1580000000}
        duration={3}
        separator="."
        delay={0}
        prefix="Rp "
      >
        {({ countUpRef, start }) => <p ref={countUpRef} className="balance" />}
      </CountUp>
      <button>Top Up</button>
    </Wrapper>
  );
};

export default Balance;
