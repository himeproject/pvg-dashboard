import styled from "styled-components";

export const Wrapper = styled.div`
  position: relative;
  background: #ffffff;
  box-shadow: 0px 0px 26px #dddfff;
  border-radius: 12px;
  margin: 48px 0 0;
  padding: 16px 19px 26px;

  > figure {
    position: absolute;
    right: 26px;
    top: 19px;
    cursor: pointer;
  }

  > .title {
    font-style: normal;
    font-weight: 700;
    font-size: 18px;
    line-height: 25px;
    text-align: center;
    color: #7a7a7a;
    margin: 0 0 24px;
  }

  > .balance {
    font-style: normal;
    font-weight: 700;
    font-size: 32px;
    line-height: 44px;
    text-align: center;
    color: #000000;
    margin: 0 0 24px;
  }

  > button {
    width: 100%;
    background: #545dff;
    border-radius: 12px;
    padding: 15px 0;
    font-style: normal;
    font-weight: 700;
    font-size: 18px;
    line-height: 25px;
    color: #ffffff;
    border: none;
    cursor: pointer;
  }
`;
