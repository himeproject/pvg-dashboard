import { WrapperProfile } from "./styles";
import Account from "./components/account";
import Balance from "./components/balance";
import React from "react";

const Profile = () => {
  return (
    <WrapperProfile>
      <Account />
      <Balance />
    </WrapperProfile>
  );
};

export default Profile;
