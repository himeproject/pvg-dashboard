import styled from "styled-components";

export const WrapperProfile = styled.div`
  height: 100%;
  background: #f3f4ff;
  width: 25%;
  padding: 40px 32px;

  @media only screen and (max-width: 768px) {
    width: 100%;
    height: fit-content;
    margin: 42px 0 0;
  }
`;
