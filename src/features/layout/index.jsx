import { Wrapper } from "./styles";
import React from "react";

const Layout = ({ children }) => {
  return <Wrapper>{children}</Wrapper>;
};

export default Layout;
