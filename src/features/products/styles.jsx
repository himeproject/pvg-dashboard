import styled from "styled-components";

export const WrapperProducts = styled.div`
  background: #ffffff;
  box-shadow: 0px 0px 26px #f0f1ff;
  border-radius: 12px;
  padding: 16px;
  margin: 40px 0 0;
  height: calc(40% - 40px);

  > .title {
    font-style: normal;
    font-weight: 700;
    font-size: 16px;
    line-height: 22px;
    color: #7a7a7a;
    margin: 0 0 21px;
  }

  > .products {
    height: calc(100% - 32px);
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
    gap: 16px;
    overflow-y: scroll;
  }

  @media only screen and (max-width: 768px) {
    height: fit-content;
  }
`;
