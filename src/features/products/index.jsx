import { WrapperProducts } from "./styles";
import Card from "./components/Card";
import React from "react";
import maximIcon from "./assets/maxim.svg";
import ovoIcon from "./assets/ovo.svg";
import plnIcon from "./assets/pln.svg";
import telkomselIcon from "./assets/telkomsel.svg";
import threeIcon from "./assets/three.svg";

const Products = () => {
  const topProducts = [
    {
      name: "Maxim",
      price: 150000,
      icon: maximIcon,
      transactions: 120,
    },
    {
      name: "Telkomsel",
      price: 25000,
      icon: telkomselIcon,
      transactions: 98,
    },
    {
      name: "Three",
      price: 50000,
      icon: threeIcon,
      transactions: 74,
    },
    {
      name: "OVO",
      price: 100000,
      icon: ovoIcon,
      transactions: 68,
    },
    {
      name: "PLN Postpaid",
      price: "",
      icon: plnIcon,
      transactions: 50,
    },
  ];

  return (
    <WrapperProducts>
      <p className="title">Top 5 Products</p>
      <div className="products">
        {topProducts.map((product, index) => (
          <Card
            icon={product.icon}
            key={index}
            name={product.name}
            transactions={product.transactions}
            price={product.price}
          />
        ))}
      </div>
    </WrapperProducts>
  );
};

export default Products;
