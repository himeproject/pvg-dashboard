import styled from "styled-components";

export const Wrapper = styled.div`
  background: #ffffff;
  border: 3px solid #989eff;
  border-radius: 12px;
  padding: 16px 12px;
  width: 128px;

  > figure {
    display: flex;
    justify-content: center;
    margin: 0 0 4px;
  }

  > .name {
    font-style: normal;
    font-weight: 700;
    font-size: 16px;
    line-height: 22px;
    text-align: center;
    color: #545dff;
    white-space: nowrap;
  }

  > .price {
    font-style: normal;
    font-weight: 700;
    font-size: 14px;
    line-height: 19px;
    text-align: center;
    color: #7a7a7a;
  }

  > .transactions {
    display: flex;
    gap: 0 4px;

    > p,
    span {
      font-family: "Nunito Sans";
      font-style: normal;
      font-weight: 700;
      font-size: 18px;
      line-height: 25px;
      text-align: center;
      color: #212121;
    }

    > span {
      font-size: 12px;
    }
  }
`;
