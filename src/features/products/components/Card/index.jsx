import { Wrapper } from "./styles";
import CountUp from "react-countup";
import React from "react";

const Card = ({ name, price, transactions, icon }) => {
  return (
    <Wrapper>
      <figure>
        <img src={icon} alt={`${name} Icon`} />
      </figure>
      <p className="name">{name}</p>
      <CountUp start={0} end={price} duration={3} separator="." delay={0}>
        {({ countUpRef, start }) => <p ref={countUpRef} className="price" />}
      </CountUp>
      <div className="transactions">
        <CountUp
          start={0}
          end={transactions}
          duration={3}
          separator="."
          delay={0}
        >
          {({ countUpRef, start }) => <p ref={countUpRef} />}
        </CountUp>
        <span>transactions</span>
      </div>
    </Wrapper>
  );
};

export default Card;
