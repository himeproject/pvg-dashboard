import styled from "styled-components";

export const WrapperBoard = styled.div`
  width: 70%;
  padding: 0 56px;
  height: 100%;

  > .cards {
    display: flex;
    gap: 0 32px;

    > div {
      width: calc(100% / 3);
    }
  }

  > .board-bottom {
    display: flex;
    gap: 32px;
    height: calc(100% - 300px);

    > .left-bottom {
      width: calc(100% / 3 + 100% / 3 + 32px);
    }
  }

  @media only screen and (max-width: 768px) {
    width: 100%;
    height: fit-content;
    padding: 0 24px;

    > .cards {
      flex-wrap: wrap;
      gap: 24px;

      > div {
        width: 100%;
      }
    }

    > .board-bottom {
      flex-wrap: wrap;
      height: 100%;
      width: 100%;

      > .left-bottom {
        width: 100%;
      }
    }
  }
`;
