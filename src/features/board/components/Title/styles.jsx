import styled from "styled-components";

export const Wrapper = styled.div`
  padding: 32px 0 68px;

  > .title {
    font-style: normal;
    font-weight: 700;
    font-size: 30px;
    line-height: 41px;
    color: #212121;
  }

  > .date {
    font-style: normal;
    font-weight: 700;
    font-size: 18px;
    line-height: 25px;
    color: #7a7a7a;
  }
`;
