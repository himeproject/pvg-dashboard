import { Wrapper } from "./styles";
import moment from "moment";
import React from "react";

const Title = () => {
  return (
    <Wrapper>
      <p className="title">Dashboard</p>
      <p className="date">Today's date: {moment().format('ddd, D MMMM YYYY')}</p>
    </Wrapper>
  );
};

export default Title;
