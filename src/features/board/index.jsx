import { WrapperBoard } from "./styles";
import Agents from "../agents";
import Card from "../card";
import Products from "../products";
import React from "react";
import Title from "./components/Title";
import Chart from "../chart";

const Board = () => {
  const cards = [
    {
      title: "Profit",
      total: 11500000,
      status: "down",
      statusText: "0.3% compared to 7 days ago",
    },
    {
      title: "Sales",
      total: 5600000,
      status: "up",
      statusText: "0.5% compared to 7 days ago",
    },
    {
      title: "Transaction",
      total: 1090,
      status: "stable",
      statusText: "same as 7 days ago",
    },
  ];

  return (
    <WrapperBoard>
      <Title />
      <div className="cards">
        {cards.map((card, index) => (
          <Card
            title={card.title}
            total={card.total}
            status={card.status}
            key={index}
            statusText={card.statusText}
          />
        ))}
      </div>
      <div className="board-bottom">
        <div className="left-bottom">
          <Chart />
          <Products />
        </div>
        <Agents />
      </div>
    </WrapperBoard>
  );
};

export default Board;
