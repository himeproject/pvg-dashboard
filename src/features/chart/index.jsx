import {
  AreaChart,
  YAxis,
  XAxis,
  CartesianGrid,
  Area,
  Tooltip,
  ResponsiveContainer,
} from "recharts";
import React from "react";
import { Wrapper } from "./styles";

const Chart = () => {
  const data = [
    {
      name: "04 April 22",
      sales: 20000000,
    },
    {
      name: "06 April 22",
      sales: 42000000,
    },
    {
      name: "08April 22",
      sales: 40000000,
    },
    {
      name: "10 April 22",
      sales: 45000000,
    },
  ];

  return (
    <Wrapper>
      <p className="title">Sales Chart</p>
      <ResponsiveContainer width={"100%"} height={"100%"}>
        <AreaChart
          width={730}
          height={250}
          data={data}
          margin={{ top: 10, right: 30, left: 0, bottom: 0 }}
        >
          <defs>
            <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
              <stop offset="5%" stopColor="#8884d8" stopOpacity={0.8} />
              <stop offset="95%" stopColor="#8884d8" stopOpacity={0} />
            </linearGradient>
            <linearGradient id="colorPv" x1="0" y1="0" x2="0" y2="1">
              <stop offset="5%" stopColor="#82ca9d" stopOpacity={0.8} />
              <stop offset="95%" stopColor="#82ca9d" stopOpacity={0} />
            </linearGradient>
          </defs>
          <XAxis dataKey="name" />
          <YAxis width={100} />
          <CartesianGrid strokeDasharray="3 3" vertical={false} />
          <Tooltip />
          <Area
            type="monotone"
            dataKey="sales"
            stroke="#545DFF"
            fillOpacity={1}
            fill="url(#colorUv)"
          />
        </AreaChart>
      </ResponsiveContainer>
    </Wrapper>
  );
};

export default Chart;
