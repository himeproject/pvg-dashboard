import styled from "styled-components";

export const Wrapper = styled.div`
  width: 100%;
  height: calc(60% - 40px);
  background: #ffffff;
  box-shadow: 0px 0px 26px #f0f1ff;
  border-radius: 12px;
  padding: 19px 16px 48px;
  margin: 40px 0 0;

  > .title {
    font-style: normal;
    font-weight: 700;
    font-size: 16px;
    line-height: 22px;
    color: #7a7a7a;
    margin: 0 0 21px;
  }

  @media only screen and (max-width: 768px) {
    height: 400px;
  }
`;
