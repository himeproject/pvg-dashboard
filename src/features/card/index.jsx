import { WrapperCard } from "./styles";
import CountUp from "react-countup";
import React from "react";
import downIcon from "./assets/down.svg";
import sameIcon from "./assets/same.svg";
import upIcon from "./assets/up.svg";

const Card = ({ title, total, status, statusText }) => {
  const currentStatus = () => {
    let color = "";
    let icon = "";

    switch (status) {
      case "down":
        color = "#E94948";
        icon = downIcon;

        return {
          color,
          icon,
        };
      case "up":
        color = "#36D189";
        icon = upIcon;

        return {
          color,
          icon,
        };
      default:
        color = "#FFDC7D";
        icon = sameIcon;

        return {
          color,
          icon,
        };
    }
  };

  return (
    <WrapperCard>
      <p className="title">{title}</p>
      <div className="total">
        <CountUp
          start={0}
          end={total}
          duration={3}
          separator="."
          delay={0}
          prefix={title !== "Transaction" ? "Rp " : ""}
        >
          {({ countUpRef, start }) => <p ref={countUpRef} />}
        </CountUp>
        {title === "Transaction" && <span>transactions</span>}
      </div>
      <div className="status">
        <img src={currentStatus().icon} alt="Status Icon" />
        <p style={{ color: currentStatus().color }}>{statusText}</p>
      </div>
    </WrapperCard>
  );
};

export default Card;
