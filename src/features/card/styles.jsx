import styled from "styled-components";

export const WrapperCard = styled.div`
  background: #ffffff;
  box-shadow: 0px 0px 26px #f0f1ff;
  border-radius: 12px;
  padding: 16px;

  > .title {
    font-style: normal;
    font-weight: 700;
    font-size: 16px;
    line-height: 22px;
    color: #7a7a7a;
  }

  > .total {
    display: flex;
    align-items: baseline;

    > p,
    span {
      font-style: normal;
      font-weight: 700;
      font-size: 30px;
      line-height: 41px;
      color: #000000;
      white-space: nowrap;
    }

    > span {
      margin: 0 4px 0;
      font-size: 20px;
    }
  }

  > .status {
    display: flex;
    align-items: center;
    justify-content: flex-end;
    gap: 0 6px;

    > p {
      font-style: normal;
      font-weight: 700;
      font-size: 14px;
      line-height: 19px;
    }
  }

  @media only screen and (max-width: 768px) {
    width: 100%;
  }
`;
