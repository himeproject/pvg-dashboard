import { createGlobalStyle } from "styled-components";

const Theme = createGlobalStyle`
	* {
		margin: 0;
		box-sizing: border-box;
	}
	
	body {
		font-family: 'Nunito Sans', sans-serif;
		padding: 0;
		margin: 0;
		box-sizing: border-box;
	}

	a {
		text-decoration: none;
		color: inherit;
	}

	a:hover {
		color: inherit;
	}
`;

export default Theme;
