import LandingPage from "../pages/landing";
import React from "react";
import Theme from "./Theme";

const App = () => {
  return (
    <>
      <Theme />
      <LandingPage />
    </>
  );
};

export default App;
